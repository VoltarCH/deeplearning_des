# DeepLearning_DES

This is the coded used in [Müller & Schnider (2021)](https://arxiv.org/abs/2102.12776) for training a convolutional
neural network to classify low-surface brightness galaxies. 

## Abstract of the paper
Countless low-surface brightness objects – including spiral galaxies, dwarf galaxies, and noise patterns – have been detected in recent large surveys. Classically, astronomers visually inspect those detections to distinguish
between real low-surface brightness galaxies and artefacts. Employing the Dark Energy Survey (DES) and ma-chine learning techniques, Tanoglidis et al. (2020) have shown how this task can be automatically performed
by computers. Here, we build upon their pioneering work and further separate the detected low-surface brightness galaxies into spirals, dwarf ellipticals, and dwarf irregular galaxies.  For this purpose we have manually
classified 5567 detections from multi-band images from DES and searched for a neural network architecture
capable of this task.  Employing a hyperparameter search, we find a family of convolutional neural networks
achieving similar results as with the manual classification, with an accuracy of 85% for spiral galaxies, 94%for dwarf ellipticals, and 52% for dwarf irregulars. For dwarf irregulars – due to their diversity in morphology– the task is difficult for humans and machines alike. Our simple architecture shows that machine learning can
reduce the workload of astronomers studying large data sets by orders of magnitudes, as will be available in
the near future with missions such as Euclid.

## Network architecture

![network architecture: 5 convolutional layers, 1 flatten, 2 fully connecte layers](nn.png "The CNN")

## Hyperparameters and Training
The code for hyperparameter search and network training can be found in src/dwarf_train.py.

## Data

The image files can be acquired using the File_Creation.ipynb script in https://github.com/dtanoglidis/DeepShadows. 
There will be two folders, artifacts and LSBs, which you need to put under input/data/. 
In the input file there are annotation*.csv files. Those are the manually classified labels, with annotation_merged.csv containing the final label (target), as well as
the three individual votes (target1, target2, target3). The manual classification was done with the src/classify.ipynb script.

## Requirements

The requirements to run our code are in the requirements.txt file.


## Citation

If you use this work, please cite:

```
@ARTICLE{2021arXiv210212776M,
       author = {{M{\"u}ller}, Oliver and {Schnider}, Eva},
        title = "{Dwarfs from the Dark (Energy Survey): a machine learning approach to classify dwarf galaxies from multi-band image}",
      journal = {arXiv e-prints},
     keywords = {Astrophysics - Astrophysics of Galaxies, Astrophysics - Instrumentation and Methods for Astrophysics},
         year = 2021,
        month = feb,
          eid = {arXiv:2102.12776},
        pages = {arXiv:2102.12776},
archivePrefix = {arXiv},
       eprint = {2102.12776},
 primaryClass = {astro-ph.GA},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2021arXiv210212776M},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```