import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


annotations=pd.read_csv('../input/annotations_merged.csv')

#print(annotations)

plt.hist(annotations["target"])
plt.hist(annotations["target1"]+0.25)
plt.hist(annotations["target2"]+0.5)
plt.hist(annotations["target3"]+0.75)
plt.show()


dE_100_percent=0
dE_all=0

dIrr_100_percent=0
dIrr_all=0

S_100_percent=0
S_all=0

dwarf_percent=0
dwarf_all=0


for index, row in annotations.iterrows():
    # check if 100% agreement
    if row["target"]==0:
        S_all+=1
        if row["target1"]+row["target2"]+row["target3"]==0:
            S_100_percent+=1

    if row["target"]==1:
        dE_all+=1
        if row["target1"]==1 and row["target2"]==1 and row["target3"]==1:
            dE_100_percent+=1

    if row["target"]==2:
        dIrr_all+=1
        if row["target1"]+row["target2"]+row["target3"]==6:
            dIrr_100_percent+=1

    if row["target"]==1 or row["target"]==2:
        dwarf_all+=1
        if (row["target1"] == 1 or row["target1"] == 2) and (row["target2"] == 1 or row["target2"] == 2)  and (row["target3"] == 1 or row["target3"] == 2):
            dwarf_percent+=1


print(S_100_percent/S_all)
print(dE_100_percent/dE_all)
print(dIrr_100_percent/dIrr_all)
print(dwarf_percent/dwarf_all)