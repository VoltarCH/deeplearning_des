import albumentations
import numpy as np
import torch
from PIL import Image
from torch.utils.data import Dataset


class DesDataset(Dataset):
    def __init__(self, path, labels, image_size=64, tfms=None):
        self.X = path
        self.y = labels
        # apply augmentations
        if tfms == 0:  # if validating
            self.aug = albumentations.Compose([
                albumentations.CenterCrop(image_size, image_size, always_apply=True), #224
                albumentations.HorizontalFlip(p=0.5),
                albumentations.RandomRotate90(always_apply=True)
            ])
        else:  # if training
            self.aug = albumentations.Compose([
                albumentations.CenterCrop(image_size, image_size, always_apply=True),
                albumentations.HorizontalFlip(p=0.5),
                albumentations.Rotate(limit=(-90,90),p=1,always_apply=True)
                #albumentations.RandomRotate90(always_apply=True)
            ])

    def __len__(self):
        return (len(self.X))

    def __getitem__(self, i):
        image = Image.open(self.X[i])
        image = self.aug(image=np.array(image))['image']
        image = np.transpose(image, (2, 0, 1)).astype(np.int)
        label = self.y[i]
        return torch.tensor(image, dtype=torch.float), torch.tensor(label, dtype=torch.long)