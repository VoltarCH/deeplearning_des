import glob
import logging
import os
import random
import sys
import time
from enum import Enum

import joblib
import matplotlib.pyplot as plt
import numpy as np
import optuna
import optuna.visualization as optuna_viz
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from PIL import Image
from PIL import ImageStat
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader
from tqdm import tqdm

from src.kidsDataset import kidsDataset
from src.models import CnnModel, resnet_model
from src.desDataset import DesDataset
PATH_TO_STUDY = "/home/voltar/PycharmProjects/deeplearning_des/src/"


if __name__ == '__main__':

    print("is cuda available",  torch.cuda.is_available())
    start = time.time()

    MAX_EPOCH_HYPERPARAMETERSEARCH = 30
    MAX_EPOCH_EPOCHSEARCH = 500
    id = "20230201"  # "20210218"
    study_name = str(MAX_EPOCH_HYPERPARAMETERSEARCH) + "_epoch_" + str(id) + "_id"

    model_path = "../outputs/model.pth"

    study = joblib.load(PATH_TO_STUDY + study_name + ".pkl")


    model = CnnModel(study.best_params["image size"], number_of_conv_layers=study.best_params["# conv layers"],
                     initial_chanels=study.best_params["initial chanels"],
                     number_of_nodes_in_hidden_fully=[study.best_params["# hidden nodes"]],
                     negative_slope=study.best_params["negative_slope"], drop_out=study.best_params["drop out"],
                     nr_of_labels=3)
    model.load_state_dict(torch.load(model_path))
    model.eval()

    device = ('cuda:0' if torch.cuda.is_available() else 'cpu')
    model.to(device)

    image_paths=glob.glob("../input/data/LSBs_KIDS/*.jpg")
    dataset = kidsDataset(image_paths,  image_size=study.best_params["image size"])
    loader = DataLoader(dataset)


    with torch.no_grad():
        for image, path in loader:
            outputs = model(image.to(device))
            _, predicted = torch.max(outputs, 1)
            print(path, predicted)

