import logging
import os
import random
import sys
import time
from enum import Enum

import joblib
import matplotlib.pyplot as plt
import numpy as np
import optuna
import optuna.visualization as optuna_viz
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from PIL import Image
from PIL import ImageStat
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader
from tqdm import tqdm

from src.models import CnnModel, resnet_model
from src.desDataset import DesDataset

PATH_TO_STUDY = "/home/voltar/PycharmProjects/deeplearning_des/src/"

''' SEED Everything '''


def seed_everything(SEED=41):
    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)
    torch.cuda.manual_seed(SEED)
    torch.cuda.manual_seed_all(SEED)
    torch.backends.cudnn.benchmark = True


# validation function
def validate(model, test_dataloader, nr_of_data, device, criterion):
    model.eval()
    val_running_loss = 0.0
    val_running_correct = 0
    with torch.no_grad():
        for i, data in tqdm(enumerate(test_dataloader), total=int(nr_of_data / test_dataloader.batch_size)):
            data, target = data[0].to(device), data[1].to(device)
            outputs = model(data)
            loss = criterion(outputs, target)

            val_running_loss += loss.item()
            _, preds = torch.max(outputs.data, 1)
            val_running_correct += (preds == target).nansum().item()

        val_loss = val_running_loss / len(test_dataloader.dataset)
        val_accuracy = 100. * val_running_correct / len(test_dataloader.dataset)
        logger.info(f'Val Loss: {val_loss:.4f}, Val Acc: {val_accuracy:.2f}')

        return val_loss, val_accuracy


# training function
def fit(model, train_dataloader, nr_of_data, device, optimizer, criterion):
    model.train()
    train_running_loss = 0.0
    train_running_correct = 0
    for i, data in tqdm(enumerate(train_dataloader), total=int(nr_of_data / train_dataloader.batch_size)):
        data, target = data[0].to(device), data[1].to(device)
        optimizer.zero_grad()
        outputs = model(data)
        loss = criterion(outputs, target)

        train_running_loss += loss.item()
        _, preds = torch.max(outputs.data, 1)
        train_running_correct += (preds == target).nansum().item()
        loss.backward()
        optimizer.step()

    train_loss = train_running_loss / len(train_dataloader.dataset)
    train_accuracy = 100. * train_running_correct / len(train_dataloader.dataset)

    logger.info(f"Train Loss: {train_loss:.4f}, Train Acc: {train_accuracy:.2f}")

    return train_loss, train_accuracy


def plot_results(train_accuracy, val_accuracy, train_loss, val_loss):
    # accuracy plots
    plt.figure(figsize=(14, 7))
    plt.subplot(121)
    plt.plot(train_accuracy, color='green', label='train accuracy')
    plt.plot(val_accuracy, color='blue', label='validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.savefig('../outputs/accuracy.png')
    # plt.show()
    # loss plots
    plt.subplot(122)
    plt.plot(train_loss, color='orange', label='train loss')
    plt.plot(val_loss, color='red', label='validation loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig('../outputs/loss.png')
    plt.show()


def imshow(img, labels):
    # img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy().astype(np.int)
    fig = plt.figure()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


def run(model, image_size, model_path, batch_size, epochs, optimizer, validation=False, trial=None, with_save=True,
        with_confusion_matrix=False):
    SEED = 42
    seed_everything(SEED=SEED)
    ''' SEED Everything '''

    # set computation device
    device = ('cuda:0' if torch.cuda.is_available() else 'cpu')
    logger.info(f"Computation device: {device}")

    # read the data (image paths and labels)
    df = pd.read_csv('../input/annotations_merged.csv')
    X = df.image_path.values # image paths
    y = df.target.values # labels

    # split the data into training, validation, and test into 70 - 10 - 20 percent
    (xtrain, xvalandtest, ytrain, yvalandtest) = (train_test_split(X, y,
                                                                   test_size=0.3, random_state=SEED))

    (xval, xtest, yval, ytest) = (train_test_split(xvalandtest, yvalandtest,
                                                   test_size=0.666, random_state=SEED))

    train_data = DesDataset(xtrain, ytrain, image_size=image_size, tfms=1)
    logger.info("number of training data:" + str(len(train_data)))


    # dataloaders
    trainloader = DataLoader(train_data, batch_size=batch_size, shuffle=True)
    if validation:
        test_data = DesDataset(xval, yval, image_size=image_size, tfms=0)
        testloader = DataLoader(test_data, batch_size=batch_size, shuffle=True)
    else:
        test_data = DesDataset(xtest, ytest, image_size=image_size, tfms=0)
        testloader = DataLoader(test_data, batch_size=batch_size, shuffle=False)

    model.to(device)

    # loss function
    criterion = nn.CrossEntropyLoss()

    # this is the training and validation
    train_loss, train_accuracy = [], []
    val_loss, val_accuracy = [], []
    start_time = time.time()
    for epoch in range(epochs):
        logger.info(f"Epoch {epoch + 1} of {epochs}")
        train_epoch_loss, train_epoch_accuracy = fit(model, trainloader, len(train_data), device, optimizer, criterion)
        val_epoch_loss, val_epoch_accuracy = validate(model, testloader, len(test_data), device, criterion)
        train_loss.append(train_epoch_loss)
        train_accuracy.append(train_epoch_accuracy)
        val_loss.append(val_epoch_loss)
        val_accuracy.append(val_epoch_accuracy)
        if trial is not None:
            trial.report(val_epoch_accuracy, epoch)
            # Handle pruning based on the intermediate value.
            if trial.should_prune():
                raise optuna.exceptions.TrialPruned()

    end_time = time.time()
    logger.info(f"{(end_time - start_time) / 60:.3f} minutes")

    if with_save:
        # save the model to disk
        logger.info('Saving model...')
        torch.save(model.state_dict(), model_path)

    class_correct = list(0. for i in range(3))
    class_total = list(0. for i in range(3))

    logger.info("number of test data:" + str(len(test_data)))
    with torch.no_grad():
        for data in testloader:
            images, labels = data
            labels = labels.to(device)
            outputs = model(images.to(device))
            _, predicted = torch.max(outputs, 1)
            c = (predicted == labels).squeeze()
            if len(c.shape) > 0:
                for i in np.arange(0, len(c)):
                    if c[i].item():
                        class_correct[labels[i]] += 1
                    class_total[labels[i]] += 1


    classes = ('spiral', 'dwarf elliptical', 'dwarf irregular')

    logger.info("number of training data:" + str(len(train_data)))
    for i in range(len(classes)):
        logger.info('Accuracy of %5s : %2d %% with %2d samples. Expected from random: %2d %%' % (classes[i],
                                                                                                 100 * class_correct[
                                                                                                     i] / class_total[
                                                                                                     i],
                                                                                                 class_total[i],
                                                                                                 class_total[
                                                                                                     i] / np.sum(
                                                                                                     class_total) * 100)
                    )

    if with_confusion_matrix:
        confusion_matix_loader = DataLoader(test_data, batch_size=len(test_data), shuffle=False)

        with torch.no_grad():
            for data in confusion_matix_loader:
                images, labels = data
                y_true = labels
                outputs = model(images.to(device))
                _, predicted = torch.max(outputs, 1)
                y_predicted = predicted
        logger.info(confusion_matrix(y_true.cpu(), y_predicted.cpu(), normalize="true"))

    return train_accuracy, val_accuracy, train_loss, val_loss


def objective(trial: optuna.trial.Trial) -> float:
    """
    This is the method Optuna needs to run the hyperparameter search.
    :param trial: Trial object from Optuna
    :return: the objective value (in our case the validation accuracy, averaged over 5 epochs)
    """
    torch.cuda.empty_cache()

    # read the number of different labels and their names
    logger.info('Loading label binarizer...')
    lb = joblib.load('../outputs/lb_spiral_dE_dIrr.pkl')
    nr_of_labels = len(lb.classes_)
    logger.info("number of labels: " + str(nr_of_labels))

    # here we define the space of the hyperparameters.
    negative_slope = trial.suggest_float("negative_slope", 0, 1)
    drop_out = trial.suggest_float("drop out", 0, 0.9)
    initial_chanels = trial.suggest_int("initial chanels", low=2, high=128, step=2)
    number_of_conv_layers = trial.suggest_int("# conv layers", 1, 5)
    number_of_nodes_in_hidden_fully = trial.suggest_int("# hidden nodes", 10, 200)
    batch_size = trial.suggest_int("batch size", low=2, high=128, step=2)
    learning_rate = trial.suggest_float("learning rate", 1e-5, 1e-1, log=True)
    weight_decay = trial.suggest_float("weight decay", 0, 0.2)
    momentum = trial.suggest_float("momentum", 0, 0.99)
    image_size = trial.suggest_int("image size", low=32, high=256, step=32)
    optimizer_name = trial.suggest_categorical("optimizer", ["Adam", "RMSprop", "SGD"])

    model = CnnModel(image_size, number_of_conv_layers=number_of_conv_layers, initial_chanels=initial_chanels,
                     number_of_nodes_in_hidden_fully=number_of_nodes_in_hidden_fully, negative_slope=negative_slope,
                     drop_out=drop_out, nr_of_labels=nr_of_labels)

    if optimizer_name == "SGD" or optimizer_name == "RMSprop":
        optimizer = getattr(optim, optimizer_name)(model.parameters(), lr=learning_rate, momentum=momentum,
                                                   weight_decay=weight_decay)
    else:
        optimizer = optim.SGD(model.parameters(), lr=learning_rate)

    model_path = "../outputs/model.pth"

    try:
        train_accuracy, val_accuracy, train_loss, val_loss = run(model, image_size, model_path, batch_size,
                                                                 MAX_EPOCH_HYPERPARAMETERSEARCH, optimizer,
                                                                 validation=True, trial=trial)
        #
        if len(val_accuracy) > 4:
            return np.mean(val_accuracy[-5:])
        else:
            return val_accuracy[-1]
    except RuntimeError as e:
        logger.info(e)
        return 0.0


def objective_res(trial: optuna.trial.Trial) -> float:
    """
    This is the method Optuna needs to run the hyperparameter search for the resnet50.
    :param trial: Trial object from Optuna
    :return: the objective value (in our case the validation accuracy, averaged over 5 epochs)
    """
    torch.cuda.empty_cache()

    logger.info('Loading label binarizer...')
    lb = joblib.load('../outputs/lb_spiral_dE_dIrr.pkl')
    nr_of_labels = len(lb.classes_)
    logger.info("number of labels:" + str(nr_of_labels))

    batch_size = trial.suggest_int("batch size", low=2, high=128, step=2)
    learning_rate = trial.suggest_float("learning rate", 1e-5, 1e-1, log=True)
    weight_decay = trial.suggest_float("weight decay", 0, 0.2)
    momentum = trial.suggest_float("momentum", 0, 0.99)
    optimizer_name = trial.suggest_categorical("optimizer", ["Adam", "RMSprop", "SGD"])

    model = resnet_model(pretrained=True, requires_grad=False, nr_of_labels=3)

    if optimizer_name == "SGD" or optimizer_name == "RMSprop":
        optimizer = getattr(optim, optimizer_name)(model.parameters(), lr=learning_rate, momentum=momentum,
                                                   weight_decay=weight_decay)
    else:
        optimizer = optim.SGD(model.parameters(), lr=learning_rate)

    model_path = "../outputs/model.pth"

    try:
        train_accuracy, val_accuracy, train_loss, val_loss = run(model, 256, model_path, batch_size,
                                                                 MAX_EPOCH_HYPERPARAMETERSEARCH, optimizer,
                                                                 validation=True, trial=trial, with_save=False)
        if len(val_accuracy) > 4:
            return np.mean(val_accuracy[-5:])
        else:
            return val_accuracy[-1]
    except RuntimeError as e:
        logger.info(e)
        return 0.0


def hyperparameter_search():
    if not os.path.isfile(PATH_TO_STUDY + study_name + ".pkl"):
        study = optuna.create_study(direction="maximize", study_name=study_name,
                                    pruner=optuna.pruners.HyperbandPruner())
    else:
        study = joblib.load(PATH_TO_STUDY + study_name + ".pkl")

    study.optimize(objective, n_trials=n_trials)
    pruned_trials = [t for t in study.trials if t.state == optuna.trial.TrialState.PRUNED]
    complete_trials = [t for t in study.trials if t.state == optuna.trial.TrialState.COMPLETE]
    joblib.dump(study, study_name + '.pkl')
    optuna_viz.plot_param_importances(study).show()
    optuna_viz.plot_intermediate_values(study).show()
    optuna_viz.plot_parallel_coordinate(study, params=["negative_slope", "drop out", "initial chanels", "# conv layers",
                                                       "# hidden nodes", "learning rate", "weight decay", "momentum",
                                                       "optimizer", "image size"]).show()
    logger.info("Study statistics: ")
    logger.info("  Number of finished trials: " + str(len(study.trials)))
    logger.info("  Number of pruned trials: " + str(len(pruned_trials)))
    logger.info("  Number of complete trials: " + str(len(complete_trials)))
    logger.info("Best trial:")
    trial = study.best_trial
    logger.info("  Value: " + str(trial.value))
    logger.info("  Params: ")
    for key, value in trial.params.items():
        logger.info("    {}: {}".format(key, value))


def hyperparameter_search_resnet():
    if not os.path.isfile(PATH_TO_STUDY + study_name + ".pkl"):
        study = optuna.create_study(direction="maximize", study_name=study_name,
                                    pruner=optuna.pruners.HyperbandPruner())
    else:
        study = joblib.load(PATH_TO_STUDY + study_name + ".pkl")

    study.optimize(objective_res, n_trials=n_trials)
    pruned_trials = [t for t in study.trials if t.state == optuna.trial.TrialState.PRUNED]
    complete_trials = [t for t in study.trials if t.state == optuna.trial.TrialState.COMPLETE]
    joblib.dump(study, study_name + '.pkl')
    optuna_viz.plot_param_importances(study).show()
    optuna_viz.plot_intermediate_values(study).show()
    optuna_viz.plot_parallel_coordinate(study, params=["batch size", "learning rate", "weight decay", "momentum",
                                                       "optimizer"]).show()
    logger.info("Study statistics: ")
    logger.info("  Number of finished trials: " + str(len(study.trials)))
    logger.info("  Number of pruned trials: " + str(len(pruned_trials)))
    logger.info("  Number of complete trials: " + str(len(complete_trials)))
    logger.info("Best trial:")
    trial = study.best_trial
    logger.info("  Value: " + str(trial.value))
    logger.info("  Params: ")
    for key, value in trial.params.items():
        logger.info("    {}: {}".format(key, value))


def load_study_with_hyperparameters():
    study = joblib.load(PATH_TO_STUDY + study_name + ".pkl")
    print(study.best_params)

    model = CnnModel(study.best_params["image size"], number_of_conv_layers=study.best_params["# conv layers"],
                     initial_chanels=study.best_params["initial chanels"],
                     number_of_nodes_in_hidden_fully=[study.best_params["# hidden nodes"]],
                     negative_slope=study.best_params["negative_slope"], drop_out=study.best_params["drop out"],
                     nr_of_labels=3)
    model_path = "../outputs/model.pth"

    if study.best_params["optimizer"] == "SGD" or study.best_params["optimizer"] == "RMSprop":
        optimizer = getattr(optim, study.best_params["optimizer"])(model.parameters(),
                                                                   lr=study.best_params["learning rate"],
                                                                   momentum=study.best_params["momentum"],
                                                                   weight_decay=study.best_params["weight decay"])
    else:
        optimizer = optim.SGD(model.parameters(), lr=study.best_params["learning rate"])
    return study, optimizer, model, model_path


def epoch_search():
    study, optimizer, model, model_path = load_study_with_hyperparameters()
    train_accuracy, val_accuracy, train_loss, val_loss = run(model, study.best_params["image size"], model_path,
                                                             study.best_params["batch size"], MAX_EPOCH_EPOCHSEARCH,
                                                             optimizer, validation=True)

    training = pd.DataFrame(
        data={"train_accuracy": train_accuracy, "val_accuracy": val_accuracy, "train_loss": train_loss,
              "val_loss": val_loss})
    training.to_csv("training_" + id + ".csv")
    plot_results(train_accuracy, val_accuracy, train_loss, val_loss)


def epoch_search_resnet():
    torch.cuda.empty_cache()
    model_path = "../outputs/model.pth"
    model = resnet_model(pretrained=True, requires_grad=False, nr_of_labels=3)
    train_accuracy, val_accuracy, train_loss, val_loss = run(model, image_size=256, model_path=model_path,
                                                             batch_size=44, epochs=MAX_EPOCH_EPOCHSEARCH,
                                                             optimizer=optim.SGD(model.parameters(),
                                                                                 lr=0.0006327251070468606,
                                                                                 weight_decay=0.09541642423590774,
                                                                                 momentum=0.9175126107733866),
                                                             validation=True, with_save=False)

    training = pd.DataFrame(
        data={"train_accuracy": train_accuracy, "val_accuracy": val_accuracy, "train_loss": train_loss,
              "val_loss": val_loss})
    training.to_csv("training_res.csv")
    plot_results(train_accuracy, val_accuracy, train_loss, val_loss)


def training_with_fixed_hyperparameters_epochs():
    study, optimizer, model, model_path = load_study_with_hyperparameters()
    train_accuracy, val_accuracy, train_loss, val_loss = run(model, study.best_params["image size"], model_path,
                                                             study.best_params["batch size"], 60,
                                                             optimizer, validation=False, with_confusion_matrix=True)
    training = pd.DataFrame(
        data={"train_accuracy": train_accuracy, "val_accuracy": val_accuracy, "train_loss": train_loss,
              "val_loss": val_loss})
    training.to_csv("training" + id + "_final.csv")
    plot_results(train_accuracy, val_accuracy, train_loss, val_loss)


def training_with_fixed_hyperparameters_epochs_resnet():
    model_path = "../outputs/model.pth"
    model = resnet_model(pretrained=True, requires_grad=False, nr_of_labels=3)
    train_accuracy, val_accuracy, train_loss, val_loss = run(model, image_size=256, model_path=model_path,
                                                             batch_size=44, epochs=50,
                                                             optimizer=optim.SGD(model.parameters(),
                                                                                 lr=0.0006327251070468606,
                                                                                 weight_decay=0.09541642423590774,
                                                                                 momentum=0.9175126107733866),
                                                             validation=False, with_save=False,
                                                             with_confusion_matrix=True)
    training = pd.DataFrame(
        data={"train_accuracy": train_accuracy, "val_accuracy": val_accuracy, "train_loss": train_loss,
              "val_loss": val_loss})
    training.to_csv("training_final_res.csv")
    plot_results(train_accuracy, val_accuracy, train_loss, val_loss)


class run_option(Enum):
    HYPERPARAMETERSEARCH = "hyperparametersearch"
    EPOCHSEARCH = "epochsearch"
    TRAINING = "training"
    HYPERPARAMETERSEARCH_RESNET = "hyperparametersearch_resnet"
    EPOCHSEARCH_RESNET = "epochsearch_resnet"
    TRAINING_RESNET = "training_resnet"


if __name__ == '__main__':

    start = time.time()
    n_trials = 60
    MAX_EPOCH_HYPERPARAMETERSEARCH = 30
    MAX_EPOCH_EPOCHSEARCH = 500
    id = "20230201"  # "20210218"
    study_name = str(MAX_EPOCH_HYPERPARAMETERSEARCH) + "_epoch_" + str(id) + "_id"

    deep_learning_option = run_option.TRAINING

    # set-up logging
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)  # Setup the root logger.
    logger.addHandler(logging.FileHandler(study_name + ".log", mode="w"))
    optuna.logging.enable_propagation()  # Propagate logs to the root logger.

    if deep_learning_option == run_option.HYPERPARAMETERSEARCH:
        hyperparameter_search()
    elif deep_learning_option == run_option.EPOCHSEARCH:
        epoch_search()
    elif deep_learning_option == run_option.TRAINING:
        training_with_fixed_hyperparameters_epochs()

    elif deep_learning_option == run_option.HYPERPARAMETERSEARCH_RESNET:
        hyperparameter_search_resnet()
    elif deep_learning_option == run_option.EPOCHSEARCH_RESNET:
        epoch_search_resnet()
    elif deep_learning_option == run_option.TRAINING_RESNET:
        training_with_fixed_hyperparameters_epochs_resnet()

    end = time.time()
    logger.info(str((end - start) / 60) + "minutes")
