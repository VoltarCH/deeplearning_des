import albumentations
import numpy as np
import torch
from PIL import Image
from torch.utils.data import Dataset


class kidsDataset(Dataset):
    def __init__(self, path, image_size=64):
        self.X = path
        self.aug = albumentations.Compose([
            albumentations.CenterCrop(image_size, image_size, always_apply=True)
        ])

    def __len__(self):
        return (len(self.X))

    def __getitem__(self, i):
        image = Image.open(self.X[i])
        image = self.aug(image=np.array(image))['image']
        image = np.transpose(image, (2, 0, 1)).astype(np.int)
        return torch.tensor(image, dtype=torch.float), self.X[i]