import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
df=pd.read_csv("training.csv")



plt.plot(df["train_accuracy"],'-',color=[0,0,146/256],label="training")
plt.plot(df["val_accuracy"],color=[146/256,0,0],label="validation")
N=10
val_accuracy_run_avg=np.convolve(df["val_accuracy"], np.ones(N)/N, mode='valid')
max_val=np.max(val_accuracy_run_avg)
index_max_val=np.argmax(val_accuracy_run_avg)
print(max_val,index_max_val)
plt.plot(val_accuracy_run_avg,'-k',lw=3)
#plt.yscale("log")
plt.xlabel("epochs")
plt.ylabel("accuracy")
plt.legend(loc='lower right')
plt.ylim()
#plt.plot(np.abs(df["val_accuracy"]-df["train_accuracy"]))
plt.savefig("validation.pdf",format="pdf",bbox_inches="tight")
plt.show()