

import pandas as pd
import joblib
import numpy as np
import torch
import random
import albumentations
import matplotlib.pyplot as plt
import argparse
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import time
from PIL import Image
from PIL import ImageStat
from tqdm import tqdm
from torchvision import models as models
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from torch.utils.data import Dataset, DataLoader
import sys
import optuna
import optuna.visualization as optuna_viz
import joblib
import os
import logging

n_trials = 300
epochs = 30
id = "20210218"
study_name = str(epochs) + "_epoch_" + str(id) + "_id"
study = joblib.load("/home/voltar/PycharmProjects/deeplearning_des/src/" + study_name + ".pkl")

#for i in np.arange(0,len( study.trials)):
#    if study.trials[i].values[0]<0.1:
#         my_state=study.trials[i].state
#         my_state=optuna.trial.TrialState.FAIL
#         print(my_state)
#         study.trials[i].state= optuna.trial.TrialState.FAIL
#         print(study.trials[i].state)


fig=optuna_viz.plot_parallel_coordinate(study, params=["negative_slope", "drop out", "initial chanels", "# conv layers",
                                                   "# hidden nodes", "learning rate", "weight decay", "momentum",
                                                   "optimizer", "image size"])


fig.update_layout(
    title=None,
    #xaxis_title="X Axis Title",
    #yaxis_title="Y Axis Title",
    #legend_title="Legend Title",
    font=dict(
        #family="Courier New, monospace",
        size=10,
        color="black"
    )
)

fig.update_layout(
    xaxis = dict(
        tickangle=0
    )
)
#fig.write_image("parallel_coordinate.png",scale=5)
#fig.show()


ax=optuna_viz.matplotlib.plot_parallel_coordinate(study, params=["negative_slope", "drop out", "initial chanels", "# conv layers",
                                                   "# hidden nodes", "learning rate", "weight decay", "momentum",
                                                   "optimizer", "image size"])
ax.set_title("")
lines = ax.get_lines()
for line, j in zip(lines, list(range(len(lines)))[::-1]):
    line.set_zorder(j)
fig = plt.gcf()
fig.set_size_inches(18, 7)
fig.gca().set_title("")
plt.savefig("parallel_coordinate.pdf",format="pdf",bbox_inches="tight")

plt.show()