from imutils import paths
import time
import numpy as np
import pandas as pd
from collections import defaultdict

run2=pd.read_csv('../input/annotations_run1.csv')
run3=pd.read_csv('../input/annotations_run2.csv')
run4=pd.read_csv('../input/annotations_run3.csv')
print(len(run2),len(pd.unique(run2['image_path'])))
print(len(run3),len(pd.unique(run3['image_path'])))
print(len(run4),len(pd.unique(run4['image_path'])))


merged=pd.DataFrame()

for index, row in run2.iterrows():
    #print(index,row["image_path"])
    #print(index,row["image_path"])
    row_run2_target=int(row["target"])
    row_run3_target=int(run3.loc[run3['image_path'] == row["image_path"]].target.values)
    row_run4_target = int(run4.loc[run4['image_path'] == row["image_path"]].target.values)
    counts = np.bincount([row_run2_target,row_run3_target,row_run4_target])
    row_target=np.argmax(counts)


    merged=merged.append({"image_path":row['image_path'],"target":row_target, "target1":row_run2_target, "target2":row_run3_target, "target3": row_run4_target},ignore_index=True)
    #print(index,row["image_path"], )
    #print(row_run3_target)

print(merged)
merged.to_csv("../input/annotations_merged.csv",index=False)
#data_all=pd.DataFrame({"image_path":annotations_array[:,0], "target":annotations_array[:,1]})