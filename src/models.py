import numpy as np
from torch import nn as nn
from torchvision import models as models


class CnnModel(nn.Module):
    def __init__(self, image_size, number_of_conv_layers=3, initial_chanels=64, number_of_nodes_in_hidden_fully=84,
                 negative_slope=0, drop_out=0, nr_of_labels=3):
        super(CnnModel, self).__init__()
        if isinstance(number_of_nodes_in_hidden_fully, int)==False:
            if len(number_of_nodes_in_hidden_fully) ==1:
                number_of_nodes_in_hidden_fully = number_of_nodes_in_hidden_fully[0]
            else:
                number_of_nodes_in_hidden_fully = np.array(number_of_nodes_in_hidden_fully)
        self.initial_chanels = initial_chanels
        self.image_size = image_size
        self.number_of_conv_layers = number_of_conv_layers
        self.negative_slope = negative_slope

        # here we build up the convolutional part of the network, combining a conv and a maxpool.
        layers_conv = nn.ModuleList()
        for i in np.arange(0, number_of_conv_layers):
            if i == 0:
                layers_conv.append(nn.Conv2d(3, self.initial_chanels, 5, padding=2))

            else:
                layers_conv.append(
                    nn.Conv2d(self.initial_chanels * 2 ** (i - 1), self.initial_chanels * 2 ** i, 5, padding=2))
            layers_conv.append(nn.LeakyReLU(negative_slope=negative_slope))
            layers_conv.append(nn.MaxPool2d(2, 2))
            layers_conv.append(nn.Dropout(drop_out))
        self.layers_conv = nn.Sequential(*layers_conv)

        # for the fully connected part, we need to know the number of nodes in the last conv layer
        last_number_of_nodes_in_conv = int(self.initial_chanels * 2 ** (number_of_conv_layers - 1) * self.image_size / (
                2 ** number_of_conv_layers) * self.image_size / (2 ** number_of_conv_layers))

        # here we build the fully connected part of the network, consisting of two fully connected layers
        layers_fully = nn.ModuleList()

        layers_fully.append(nn.Linear(last_number_of_nodes_in_conv, number_of_nodes_in_hidden_fully))
        layers_fully.append(nn.LeakyReLU(negative_slope=negative_slope))

        layers_fully.append(nn.Linear(number_of_nodes_in_hidden_fully, nr_of_labels))
        layers_fully.append(nn.LeakyReLU(negative_slope=negative_slope))

        self.layers_fully = nn.Sequential(*layers_fully)

    def forward(self, x):
        x = self.layers_conv(x)  # convolutions
        x = x.view(-1, int(self.initial_chanels * 2 ** (self.number_of_conv_layers - 1) * self.image_size / (
                    2 ** (self.number_of_conv_layers)) * self.image_size / (
                                       2 ** (self.number_of_conv_layers))))  # flatten
        x = self.layers_fully(x)  # fully connected
        return x


def resnet_model(pretrained, requires_grad,nr_of_labels):
    model = models.resnext50_32x4d(progress=True, pretrained=pretrained)
    # freeze hidden layers
    if requires_grad == False:
        for param in model.parameters():
            param.requires_grad = False
    # train the hidden layers
    elif requires_grad == True:
        for param in model.parameters():
            param.requires_grad = True
    # make the classification layer learnable
    model.fc = nn.Linear(2048, nr_of_labels)
    return model